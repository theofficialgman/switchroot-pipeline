#!/bin/bash

browser_version=125.0.6422.141

# Build chromium ffmpeg
mkdir -p "${OUT}/chromium_ffmpeg/"
/usr/sbin/update-ccache-symlinks
export PATH="/usr/lib/ccache:$PATH"
apt-get update
# download ffmpeg source
mkdir /ffmpeg
cd /ffmpeg
# https://chromium.googlesource.com/chromium/src.git/+/refs/tags/121.0.6167.139/DEPS
ffmpeg_commit="$(wget -qO- https://chromium.googlesource.com/chromium/src.git/+/refs/tags/$browser_version/DEPS?format=TEXT | base64 --decode | grep "'ffmpeg_revision': " | sed "s/'ffmpeg_revision': //g" | tr -d ",'" | awk ' { print $1 }')"
echo "ffmpeg commit hash: $ffmpeg_commit"
wget https://chromium.googlesource.com/chromium/third_party/ffmpeg/+archive/$ffmpeg_commit.tar.gz -O ffmpeg.tar.gz
tar -xvf ffmpeg.tar.gz
rm -f ffmpeg.tar.gz

# patch ffmpeg
patch -p1 -i /build/patches/userspace/chromium/nvv4l2-chrome-125.0.patch

chmod +x ./build_for_chrome.sh
./build_for_chrome.sh
strip libffmpeg.so
cp libffmpeg.so ${OUT}/chromium_ffmpeg/
cd ../

cp ${OUT}/chromium_ffmpeg/*.so .

setup_patched() {
        apt update -y
        case "$1" in
        chromium-browser) 
          wget https://github.com/theofficialgman/testing/releases/download/gmans-releases/chromium-browser-stable_$browser_version-1_arm64.deb -O chromium-browser_$browser_version-1_arm64.deb
          ;;
        esac
        mkdir -p $1
        mv $1_*.deb $1
}

extract() {
        cd $1
        ar x $1_*.deb
        rm $1_*.deb
        tar xf data.tar.xz
        mkdir DEBIAN
        tar xf control.tar.xz -C DEBIAN
        rm -rf control.tar.xz \
                data.tar.xz \
                debian-binary \
                DEBIAN/md5sums \
                DEBIAN/archives \
                DEBIAN/conffiles \
                DEBIAN/postinst \
                DEBIAN/postrm \
                DEBIAN/prerm \
                etc/cron.daily || true
}

repack() {
        sed -i 's/'${version}'/9:'${version}'/g' DEBIAN/control
        sed -i 's/Package: chromium-browser-stable/Package: chromium-browser/g' DEBIAN/control
        # First version with merged locales and ffmpeg is 9:119.0.6045.105-1. No need to bump the versions below in the future.
        echo 'Breaks: chromium-browser-l10n (<< 9:119.0.6045.105-1), chromium-chromedriver (<< 9:119.0.6045.105-1), chromium-codecs-ffmpeg-extra (<< 9:119.0.6045.105-1), chromium-codecs-ffmpeg (<< 9:119.0.6045.105-1)' >> DEBIAN/control
        echo 'Replaces: chromium-browser-l10n (<< 9:119.0.6045.105-1), chromium-chromedriver (<< 9:119.0.6045.105-1), chromium-codecs-ffmpeg-extra (<< 9:119.0.6045.105-1), chromium-codecs-ffmpeg (<< 9:119.0.6045.105-1)' >> DEBIAN/control
        sed -i '/Provides:/d' DEBIAN/control
        sed -i '/Maintainer:/d' DEBIAN/control
        echo 'Provides: www-browser, chromium-browser-l10n, chromium-codecs-ffmpeg-extra' >> DEBIAN/control
        echo 'Maintainer: theofficialgman <dofficialgman@gmail.com>' >> DEBIAN/control
        # ultra chromium deb compression method https://source.chromium.org/chromium/chromium/src/+/refs/tags/122.0.6261.57:chrome/installer/linux/debian/build.sh;l=120-129;bpv=0
        # reduces size from ~93 MB via standard methods to ~82 MB
        mkdir ../$1-ultra-compressed
        dpkg-deb -Znone -b . ../$1-ultra-compressed/$1_9:${version}_arm64.deb
        cd ../$1-ultra-compressed
        ar -x $1_9:${version}_arm64.deb
        xz -z9 -T0 --lzma2='dict=256MiB' data.tar
        xz -z0 control.tar
        ar -d $1_9:${version}_arm64.deb control.tar data.tar
        ar -r $1_9:${version}_arm64.deb control.tar.xz data.tar.xz
        mv $1_9:${version}_arm64.deb ../
        cd ..
        rm -rf $1 $1-ultra-compressed
}

# use chromium-browser with libvpx disabled and ffmpeg based vp9 decoding enabled
# https://github.com/theofficialgman/testing/releases/tag/gmans-releases

# chromium-browser
setup_patched chromium-browser
extract chromium-browser

export version=$(grep "^Version: .*$" DEBIAN/control | sed 's/Version: //g')

mkdir -p usr/ opt/ etc/ DEBIAN/
cp -r $(dirname -- "${BASH_SOURCE[0]}")/assets/chromium/usr/* usr/
cp -r $(dirname -- "${BASH_SOURCE[0]}")/assets/chromium/opt/* opt/
cp -r $(dirname -- "${BASH_SOURCE[0]}")/assets/chromium/etc/* etc/
cp -r $(dirname -- "${BASH_SOURCE[0]}")/assets/chromium/DEBIAN/* DEBIAN/

# add widevine binary
WD="$(pwd)"
cd opt/chromium.org/chromium/
wget https://github.com/theofficialgman/testing/releases/download/gmans-releases/WidevineCdm-2.36.tar.gz
tar -xvf WidevineCdm-2.36.tar.gz
rm WidevineCdm-2.36.tar.gz
cd "$WD"

# add ffmpeg binary
cp ../libffmpeg.so opt/chromium.org/chromium/

repack chromium-browser

rm ${OUT}/*.deb
cp *.deb ${OUT}
