#!/bin/bash

# Output dir
mkdir -p "${OUT}/glibc_bionic/"

# Setup ccache
/usr/sbin/update-ccache-symlinks
export PATH="/usr/lib/ccache:$PATH"

# Update and get sources
apt update -y
apt build-dep glibc -y
apt-get source glibc

# Get version
export VERSION_FULL=$(cat glibc*.dsc | grep '^Version:' | awk '{print $2}')
export VERSION_SHORT=$(echo $VERSION_FULL | sed 's/.*://; s/\-.*//')
export VERSION_NO_PREFIX=$(echo ${VERSION_FULL} | sed 's/.*://')

cd glibc-${VERSION_SHORT}

# Add patches to debian folder and apply once
# Some debian buildscripts are known to revert patches (chromium is an example) in the tree to test them before building
echo 'arm/glibc-2.27-sht_relr_new.diff' >> debian/patches/series
echo 'arm/glibc-tls-libwidevinecdm.so-since-4.10.2252.0-has-TLS-with.patch' >> debian/patches/series
wget https://cdn.discordapp.com/attachments/567232809475768320/1078517349776359485/glibc-2.27-sht_relr_new.diff -P debian/patches/arm/
wget https://cdn.discordapp.com/attachments/567232809475768320/1078517350137090098/glibc-tls-libwidevinecdm.so-since-4.10.2252.0-has-TLS-with.patch -P debian/patches/arm/
patch -p1 -i debian/patches/arm/glibc-2.27-sht_relr_new.diff
patch -p1 -i debian/patches/arm/glibc-tls-libwidevinecdm.so-since-4.10.2252.0-has-TLS-with.patch

# Bump version in all files in tree
find . -type f -exec sed -i 's/'${VERSION_FULL}'/'${VERSION_FULL//-3/-9}'/g' {} \;

# Build debs
DEB_BUILD_OPTIONS=nocheck DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -j"${CPUS}"
cd ..

# Copy build to output
cp -r *.deb "${OUT}/glibc_bionic/"
