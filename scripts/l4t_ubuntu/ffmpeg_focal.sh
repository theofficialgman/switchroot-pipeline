#!/bin/bash

# Output dir
mkdir -p "${OUT}/ffmpeg_focal/"

# Setup ccache
/usr/sbin/update-ccache-symlinks
export PATH="/usr/lib/ccache:$PATH"

# Get sources
apt update
apt-get source ffmpeg

# Update and get version
export VERSION_FULL=$(apt show ffmpeg | grep 'Version:' | awk '{print $2}')
export VERSION_SHORT=$(echo $VERSION_FULL | sed 's/.*://; s/\-.*//')

cd ffmpeg-${VERSION_SHORT}

# Apply nvv4l2 patches
sed -i 's/CONFIG := /CONFIG := --extra-cflags="-march=armv8-a+simd+crypto+crc -mtune=cortex-a57" \\\n\t/g' debian/rules
sed -i 's/CONFIG := /CONFIG := --enable-nvv4l2 \\\n\t/g' debian/rules
patch -p1 -i /build/patches/userspace/ffmpeg/ffmpeg_nvv4l2_4.2.4.patch

# Ignore querrying package info
echo -e "\noverride_dh_shlibdeps:\n\tdh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info" >> debian/rules

# Commit dpkg changes
EDITOR=/bin/true dpkg-source -q --commit . nvv4l2.patch

# Bump version
sed -i "s/${VERSION_FULL}/${VERSION_FULL}l4t/g" debian/changelog

# Build debs
DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -j"${CPUS}"
cd ..

# Copy build to output
cp -r *.deb "${OUT}/ffmpeg_focal/"
