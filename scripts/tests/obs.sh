#!/bin/bash
set -xe
export VER=${VER:-"37"}
export OUT=$(realpath ${OUT})

osc co home:Azkali:Tegra:Icosa

7z a -tzip \
	home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:artifacts.zip \
	${OUT}/uImage \
	${OUT}/nx-plat.dtimg \
	${OUT}/modules.tar.gz \
	${OUT}/update.tar.gz \
	${OUT}/kernel-4.9-src.7z

wget -O \
	home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:initramfs \
	https://gitlab.azka.li/l4t-community/kernel/l4t-initramfs/-/jobs/artifacts/master/raw/initramfs?job=build

cp ${OUT}/l4s-bsp-tx1-32.3.1/l4s_bsp_icosa_32.3.1.tar.gz \
	home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:l4s_bsp_icosa_32.3.1.tar.gz

cp ${OUT}/switch_configs/l4s_configs.tar.gz \
	home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:l4s_configs.tar.gz

cp ${OUT}/l4s-bsp-nano-32.3.1/l4s_bsp_nano_32.3.1.tar.gz \
    home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:l4s_bsp_nano_32.3.1.tar.gz

wget -O \
    home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:org.freedesktop.Platform.GL.nvidia-tegra-32-3-1.flatpak \
	https://raw.githubusercontent.com/cobalt2727/L4T-Megascript/master/assets/Flatpak/t210/org.freedesktop.Platform.GL.nvidia-tegra-32-3-1.flatpak

cp ${OUT}/switchroot-fedora-boot.7z \
    home\:Azkali\:Tegra\:Icosa/l4s-bsp/_service:download_url:switchroot-fedora-boot.7z

cd home\:Azkali\:Tegra\:Icosa/l4s-bsp/
osc build \
	-j $(nproc) \
	-t $(nproc) \
	--vm-type=chroot \
	--noverify \
	--noservice \
	--no-changelog \
	--nodebugpackages \
	--disable-debuginfo \
	--download-api-only \
	--trust-all-projects \
	aarch64

7z a "${OUT}/bsp.7z" /var/tmp/build-root/Fedora_37-aarch64/.mount/home/abuild/rpmbuild/RPMS/aarch64/*.rpm
