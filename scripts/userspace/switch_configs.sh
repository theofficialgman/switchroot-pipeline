#!/bin/bash

# Prepare
mkdir -p ${OUT}
cd ${OUT}

# Switch specific configs
for dir in /build/switch-l4t-configs/*; do
	dir=${dir##*/}
	if [[ ${dir} != "nvidia-l4t-bsp" ]] ;then
		mkdir -p ${dir}
		cp -r /build/switch-l4t-configs/${dir}/* ${dir}/
	fi
done

mkdir -p nvidia-l4t-bsp
cp -r /build/switch-l4t-configs/nvidia-l4t-bsp/*/* nvidia-l4t-bsp/

# Cleanup
rm -rf switch-l4t-configs.tar switchroot-pipeline-master-switch-l4t-configs/ switch-l4t-configs

echo -e "Creating l4s_configs disk image\n"
mkdir -p configs
cp switch-dock-handler/etc/skel/.config/monitors_l4t.xml switch-dock-handler/etc/skel/.config/monitors.xml
cp switch-dock-handler/root/.config/monitors_l4t.xml switch-dock-handler/root/.config/monitors.xml
rm switch-dock-handler/etc/skel/.config/monitors_l4t.xml switch-dock-handler/root/.config/monitors_l4t.xml
cp -r switch-*/* configs
cp -r nvidia-l4t-bsp/* configs
tar czf "${OUT}/l4s_configs.tar.gz" -C configs .

echo -e "Creating l4s_configs_mainline disk image\n"
mkdir -p configs_mainline
rm -rf switch-xorg-conf/etc/X11/xorg.conf.d/11-nvidia.conf
cp switch-dock-handler/etc/skel/.config/monitors_mainline.xml switch-dock-handler/etc/skel/.config/monitors.xml
cp switch-dock-handler/root/.config/monitors_mainline.xml switch-dock-handler/root/.config/monitors.xml
rm switch-dock-handler/etc/skel/.config/monitors_mainline.xml switch-dock-handler/root/.config/monitors_mainline.xml
rm -rf switch-r2c-utils switch-initial-setup-fedora switch-chromium-nvdec-assets
cp -r switch-*/* configs_mainline
tar czf "${OUT}/l4s_configs_mainline.tar.gz" -C configs_mainline .
