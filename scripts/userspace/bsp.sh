#!/bin/bash

# Variables
bsp=${bsp:-"https://developer.nvidia.com/embedded/dlc/r32-3-1_Release_v1.0/t210ref_release_aarch64/Tegra210_Linux_R32.3.1_aarch64.tbz2"}
jetson_api=${jetson_api:-"https://repo.download.nvidia.com/jetson/t210/pool/main/n/nvidia-l4t-jetson-multimedia-api/nvidia-l4t-jetson-multimedia-api_32.3.1-20191209225816_arm64.deb"}
configs=${configs:-"https://gitlab.azka.li/l4t-community/gnu-linux/switchroot-pipeline/-/archive/master/switchroot-pipeline-master.tar?path=switch-l4t-configs"}
zstd=${zstd:-"false"}

if [[ $zstd = "false" ]]; then zstd=""
else zstd="-I zstd"; fi

# Prepare
mkdir -p ${OUT}
cd ${OUT}

# Download bits
wget ${bsp}
wget ${jetson_api}

# Extract BSP
tar xf ${bsp##*/}
mv Linux_for_Tegra/bootloader/*.deb ${OUT}
mv Linux_for_Tegra/kernel/*.deb ${OUT}
mv Linux_for_Tegra/nv_tegra/l4t_deb_packages/*.deb ${OUT}
#mv Linux_for_Tegra/tools/*.deb ${OUT}

# Switchroot nvpmodel
wget ${configs} -O switch-l4t-configs.tar
tar xf switch-l4t-configs.tar -C ${OUT}
mv switchroot-pipeline-master-switch-l4t-configs/switch-l4t-configs/ ${OUT}

#
# Nvidia BSP
#
for deb in ${OUT}/*.deb
do
	# Retrieve package name
	dname=$(echo ${deb} | sed 's/_.*//g; s@.*/@@; s/nvidia-l4t-//g')

	# Create temp package dir
	mkdir -p ${dname}

	# Extract .deb
	7z x ${deb}
	tar ${zstd} -xvf data.tar.* -C ${dname}

	# Move sbin to bin and lib to usr/lib
	if [ -e ${dname}/lib ]; then
		mkdir -p ${dname}/usr/lib/
		mv ${dname}/lib/* ${dname}/usr/lib/
		rm -rf ${dname}/lib/
	fi

	# Remove unneeded
	rm -rf ${dname}/usr/lib/systemd/system/bluetooth.service.d/ \
		${dname}/_gpgbuilder \
		${deb} \
		data.* \
		control.* \
		debian-binary

	# Copy Xorg from 3d-core to lib64
	if [ "${dname}" = "3d-core" ]; then
		mkdir -p ${dname}/usr/lib64
		cp -r ${dname}/usr/lib/xorg ${dname}/usr/lib64/
	fi

	if [ "${dname}" = "core" ]; then
		echo "/usr/lib" >> ${dname}/etc/ld.so.conf.d/nvidia-tegra.conf
		echo "/usr/lib64" >> ${dname}/etc/ld.so.conf.d/nvidia-tegra.conf
		echo "/usr/lib/aarch64-linux-gnu/tegra" >> ${dname}/etc/ld.so.conf.d/nvidia-tegra.conf
		echo "/usr/lib/aarch64-linux-gnu/tegra-egl" >> ${dname}/etc/ld.so.conf.d/nvidia-tegra.conf
	fi

	if [ "${dname}" = "camera" ]; then
		rm -rf ${dname}/etc/systemd/system/multi-user.target.wants/
	fi

	# Apply switchroot background
	if [ "${dname}" = "configs" ]; then
		rm -rf ${dname}/etc/systemd/nvfb.sh \
			${dname}/etc/systemd/system/nvfb-early.service \
			${dname}/etc/systemd/system/multi-user.target.wants/nvfb-early.service \
			${dname}/etc/systemd/system/apt-daily.timer.d \
			${dname}/etc/systemd/system/apt-daily-upgrade.timer.d \
			${dname}/etc/systemd/resolved.conf.d \
			${dname}/etc/systemd/timesyncd.conf.d
	fi

	if [ "${dname}" = "init" ]; then
		# Create hostname and modules files
		echo "switchroot" > ${dname}/etc/hostname
		echo -e "bluedroid_pm\nnvhost_vi\nnvgpu\nbrcmfmac\nbtbcm" >> ${dname}/etc/modules
		rm -rf ${dname}/opt/nvidia/l4t-usb-device-mode/ \
			${dname}/etc/systemd/nvzramconfig.sh \
			${dname}/etc/systemd/system/nvmemwarning.service \
			${dname}/etc/systemd/nvfb.sh \
			${dname}/etc/systemd/nvgetty.sh \
			${dname}/etc/systemd/system/nv-l4t-usb-device-mode-runtime.service \
			${dname}/etc/systemd/system/nv-l4t-usb-device-mode.service \
			${dname}/etc/systemd/system/multi-user.target.wants/nv-l4t-usb-device-mode.service \
			${dname}/etc/systemd/system/multi-user.target.wants/nvgetty.service \
			${dname}/etc/systemd/system/nvgetty.service \
			${dname}/etc/systemd/system/nvmemwarning.service \
			${dname}/etc/systemd/system/multi-user.target.wants/nvmemwarning.service \
			${dname}/etc/systemd/system/nvfb.service \
			${dname}/etc/systemd/system/multi-user.target.wants/nvfb.service \
			${dname}/etc/systemd/system/nvzramconfig.service \
			${dname}/etc/systemd/system/multi-user.target.wants/nvzramconfig.service \
			${dname}/opt/nvidia/l4t-bootloader-config/ \
			${dname}/opt/nvidia/l4t-usb-device-mode/ \
			${dname}/etc/systemd/system/multi-user.target.wants/nv-l4t-bootloader-config.service \
			${dname}/etc/udev/rules.d/99-nv-l4t-usb-device-mode.rules \
			${dname}/etc/udev/rules.d/99-nv-ufs-mount.rules \
			${dname}/etc/udev/rules.d/99-nv-wifibt.rules 
			
			# Force enable loading mesa (glvnd); change dkpg stuffs to uname to get arch
			cp -r ${OUT}/switch-l4t-configs/nvidia-l4t-bsp/init/* ${dname}

			mkdir -p ${dname}/usr/lib/alsa/init/
			mv ${dname}/usr/share/alsa/init/postinit ${dname}/usr/lib/alsa/init/
			rm -rf ${dname}/usr/share/alsa/init
	fi

	if [ "${dname}" = "firmware" ]; then
		if [[ ${bsp_ver} == "32.7.4" ]]; then
			ln -s ../tegra21x/nv_acr_ucode_prod.bin ${dname}/usr/lib/firmware/gm20b/acr_ucode.bin
		fi

		cp -r ${OUT}/switch-l4t-configs/nvidia-l4t-bsp/firmware/* ${dname}

		rm -rf ${dname}/etc/systemd/nvwifibt-pre.sh \
			${dname}/etc/systemd/nvwifibt.sh \
			${dname}/etc/systemd/system/nvwifibt.service \
			${dname}/usr/lib/firmware/brcm/BCM.hcd
	fi

	if [ "${dname}" = "multimedia" ]; then
		mkdir -p ${dname}/usr/lib64/libv4l/plugins/
		ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4l2_nvvidconv.so ${dname}/usr/lib64/libv4l/plugins/libv4l2_nvvidconv.so
		ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4l2_nvvideocodec.so ${dname}/usr/lib64/libv4l/plugins/libv4l2_nvvideocodec.so
		ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4l2.so.0 ${dname}/usr/lib64/libv4l/plugins/libv4l2.so.0
		ln -s /usr/lib/aarch64-linux-gnu/tegra/libv4lconvert.so.0  ${dname}/usr/lib64/libv4l/plugins/libv4lconvert.so.0  
	fi

	if [ "${dname}" = "tools" ]; then
		# Create switch specific nvpmodel
		cp -r ${dname} ${dname}-swr
		dname=${dname}-swr

		cp -r ${OUT}/switch-l4t-configs/nvidia-l4t-bsp/nvpmodel/* ${dname}
		sed -i 's/OnlyShowIn.*//g' ${dname}/etc/xdg/autostart/nvpmodel_indicator.desktop
        sed -i 's/x-terminal-emulator -e/gnome-terminal --/g' ${dname}/usr/share/nvpmodel_indicator/nvpmodel_indicator.py
	fi

	# Weston symlink fix
	if [ "${dname}" = "weston" ]; then
		mkdir -p ${dname}/usr/lib64/
		ln -sf /usr/lib/aarch64-linux-gnu/tegra/weston/hmi-controller.so ${dname}/usr/lib/aarch64-linux-gnu/weston/hmi-controller.so
		ln -sf /usr/lib/aarch64-linux-gnu/tegra/weston/libweston-6.so.0 ${dname}/usr/lib64/libweston-6.so.0
		ln -sf /usr/lib/aarch64-linux-gnu/tegra/weston/libweston-desktop-6.so.0 ${dname}/usr/lib64/libweston-desktop-6.so.0
	fi
done

# Cleanup
rm -rf ${bsp##*/} Linux_for_Tegra switch-l4t-configs.tar switchroot-pipeline-master-switch-l4t-configs/ switch-l4t-configs

# Icosa specific
if [[ "${device}" = "icosa" ]]; then
	rm -rf bootloader tools
else
	rm -rf tools-swr
fi

if [[ "${device}" = "icosa" ]] || [[ "${device}" = "nano" ]]; then
	echo -e "Creating disk image\n"

	mkdir -p bsp
	cp -r */* bsp

	if [[ "${device}" = "icosa" ]]; then rm -rf bsp/boot/; fi

	tar czf "${OUT}/l4s_bsp_${device}_${bsp_ver}.tar.gz" -C bsp .
else
	7z a l4s-bsp-${device}-${bsp_ver}.7z * -x'!*.7z'
fi
